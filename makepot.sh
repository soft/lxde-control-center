#!/bin/bash
xgettext --language=Python --keyword=_ --output=po/lxde-control-center.pot lib/lxde/lxde-control-center/control-center.py translations.py --from-code=utf-8

cd po
#add you lang here
msgmerge ru.po lxde-control-center.pot -o ru.po
msgmerge fr.po lxde-control-center.pot -o fr.po
