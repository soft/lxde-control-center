#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
 Mandriva Control Center LXDE 0.1-1
 Copyright (C) 2011
 Author: Alexander Kazancev <kazancas@mandriva.ru>
 Edumandriva Team - edumandriva.ru
 
 Based on Centro de Control 0.3-5 by Mario Colque <mario@tocuito.org.ar>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
"""

import gtk
import os
import commands
import gettext
import webkit
import string
import json
from user import home

# i18n
gettext.install('lxde-control-center', '/usr/share/locale/')

class MessageDialog:
    def __init__(self, title, message, style):
        self.title = title
        self.message = message
        self.style = style

    def show(self):
        dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT, self.style, gtk.BUTTONS_OK)
        dialog.set_markup(_('<b>Error:</b>'))
        dialog.set_icon_name('preferences-desktop')
        dialog.format_secondary_markup(self.message)
        dialog.set_title(_('Control Center'))
        dialog.set_position(gtk.WIN_POS_CENTER)
        dialog.run()
        dialog.destroy()

class ControlCenter():
    def __init__(self):
        self.builder = gtk.Builder()
        self.builder.add_from_file('/usr/lib/lxde/lxde-control-center/control-center.glade')
        self.window = self.builder.get_object('window')
        self.builder.get_object('window').set_title(_('Control Center'))
        self.edit_handler_id = False
        self.add_handler_id = False
        self.items_cache = []
        self.items_advanced_cache = []
        self.theme = gtk.icon_theme_get_default()
        self.builder.get_object('window').connect('destroy', gtk.main_quit)
        self.browser = webkit.WebView()
        self.builder.get_object('window').add(self.browser)
        self.browser.connect('button-press-event', lambda w, e: e.button == 3)

        self.text = {}
        self.text['appearance'] = _('Appearance')
        self.text['network'] = _('Network & Internet')
        self.text['programs'] = _('Programs')
        self.text['user_accounts'] = _('User accounts and access control')
        self.text['accounts'] = _('User Accounts')
        self.text['system'] = _('System and security')
        self.text['hard'] = _('Hardware and sound')
        self.text['about'] = _('About')
        self.text['back'] = _('Back to menu')

        self.text['change_theme'] = _('Change the background and theme')
        self.text['visual_efects'] = _('Configure visual effects')
        self.text['resolution'] = _('Adjust the screen resolution')

        self.text['connections'] = _('Network connections')
        self.text['network_tools'] = _('Network tools')
        self.text['network_places'] = _('Network sites')

        self.text['add_users'] = _('Add or remove user accounts')
        self.text['control_parental'] = _('Set up Parental Control')

        self.text['drivers'] = _('Install drivers')
        self.text['printer'] = _('View devices and printers')

        self.text['status'] = _('Status computer')
        self.text['backup'] = _('Make a backup')

        self.text['add_programs'] = _('Add/Remove programs')
        self.text['favorites'] = _('Set favorite applications')
        self.text['trash'] = _('Remove unused files')
        self.text['problems'] = _('Find and fix problems')
        self.text['settings'] = _('Settings')
        self.text['themeswitcher'] = _('Theme switch')

        template = self.get_template()
        html = string.Template(template.read()).safe_substitute(self.text)
        template.close()
        self.browser.load_html_string(html, 'file:/')
        self.browser.connect('title-changed', self.title_changed)
        self.window.show_all()

    def set_options_status(self):
        if self.mode:
            self.text['input_mode_id'] = 'checked="checked"'
            self.text['input_mode'] = ''
        else:
            self.text['input_mode_id'] = ''
            self.text['input_mode'] = 'checked="checked"'
            self.text['input_suggestions'] = ''
            self.text['input_visual'] = ''

    def get_template(self):
        template = open('/usr/lib/lxde/lxde-control-center/frontend/default.html')
        return template

    def title_changed(self, view, frame, title):
        self.current_commands = []
        if title.startswith('exec:'):
            command = title.split(':')[1]
            cmd = commands.getoutput('which ' + command)
            if cmd != '':
                os.system('%s &' % command)
            else:
                message = MessageDialog('Error', _('The command <b>%s</b> is not found or not is executable') % command, gtk.MESSAGE_ERROR)
                message.show()
        elif title.startswith('category:'):
            self.category = title.split(':')[1]
            self.home_file = os.path.join(home, '.lxde/lxde-control-center/items/' + self.category)
            self.base_file = os.path.join('/usr/lib/lxde/lxde-control-center/items/', self.category)
            self.has_suggestions = False
            if os.path.isfile(self.home_file):
                self.category_file = self.home_file
            else:
                self.category_file = self.base_file
            items_file = open(self.category_file)
            dic = json.load(items_file)
            items_file.close()
            for k, v in dic.iteritems():
                self.current_commands.append(k)
                command = k
                title = v['title']
                owner = v['owner']
                icon = v['icon']
                if not os.path.isfile(icon):
                    if self.theme.has_icon(icon):
                        iconInfo = self.theme.lookup_icon(icon, 24, 0)
                        icon = iconInfo.get_filename()
                    else:
                        iconInfo = self.theme.lookup_icon("applications-other", 24, 0)
                        if iconInfo and os.path.exists(iconInfo.get_filename()):
                            icon = iconInfo.get_filename()
                        else:
                            icon = '/usr/lib/lxde/lxde-control-center/frontend/images/applications-other.png'
                command_clean = command.split(' ')[0]
                if command_clean != 'gksu':
                    command_search = command_clean
                else:
                     command_search = command.split(' ')[1]
                cmd = commands.getoutput('which ' + command_search)
                if cmd != '' or owner == 'user':
                    if command not in self.items_cache:
                        self.items_cache.append(command)
                        #usage: addItem(title, command, category, icon)
                        self.browser.execute_script("addItem('%s','%s','%s','%s')" % (_(title), command, self.category, icon))
                if cmd == '' and not self.has_suggestions and self.show_suggestions:
                    self.has_suggestions = True
                    self.browser.execute_script("setSuggestions('" + self.category + "', 'show')")
            self.browser.execute_script("setContent('" + self.category + "')")
        elif title == 'edit-item':
            self.items_window(self)
        elif title == 'suggestions':
            os.system('/usr/lib/lxde/lxde-control-center/suggestions.py %s &' % self.category)
        elif title == 'about':
            self.about(self)

if __name__ == '__main__':
    home_path = os.path.join(home, '.lxde/lxde-control-center/items')
    if not os.path.exists(home_path):
        os.system('mkdir -p ' + home_path)
    gtk.gdk.threads_init()
    ControlCenter()
    gtk.main()
