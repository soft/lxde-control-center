var cat, side = false;

function changeTitle(title) {
    document.title = title;
    document.title = 'nop';
}

function changeCategory(category) {
    document.title = 'category:' + category;
    document.title = 'nop';
}

function setContent(category) {
    cont = $('#' + category + '_html').html();
    $('#ajax').hide().html(cont).fadeIn();
    $('#container_main').show();
}

function addItem(title, command, category, icon) {
    li = "<li id='" + command + "' onclick='javascript:changeTitle(\"exec:" + command + "\")'><div class='icon'><img id='" + command + "' src='" + icon + "' /></div><div class='data'><h4 id='" + command + "'>" + title + "</h4></div></li>";
    $('ul#' + category + '_ul').append(li);
}

function editItem(title, old_command, new_command, icon) {
    $('li#' + old_command).attr({
        id: new_command,
        onclick: 'javascript:changeTitle("exec:' + new_command + '"\")',
    });
    $('img#' + old_command).attr({
        src: icon,
    })
    $('h4#' + old_command).attr({
    }).text(title);
}

function removeItem(command, category) {
    if (command == 'all-items')
        $('#' + category + '_ul li').remove();
    else
        $('li[id|="' + command + '"]').remove();
}

function back() {
    $('#ajax').fadeOut(50);
    $('#glavnoe').fadeIn();
    $('#side_ui').hide();
    cat = null;
    side = false;
}

function initial_start() {
                side = true;
                $('#side_ui').show();
                $('#container_main').show();
       changeCategory('appearance');       
}

$(function() {

    $('.knopka').click(function() {
			
        category = $(this).data('cat');
        if (cat != category) {
            cat = category;
            if (!side) {
                side = true;
                $('#side_ui').show();
                $('#container_main').show();
            }
                changeCategory(category);
        }
    });
});
